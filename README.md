# Sign language translator
This project is part of the second assignment of the full stack developer course at Noroff.
The goal of this assignment is to develop a single page web application with react.
We also make use of heroku deploy our own web application as well as an already provided API.
The assignment itself can be found in [the pdf](React_Lost%20in%20Translation.pdf).

## Installation
The web app itself is deployed on Heroku and is accessible via [the following link](https://mr-choi-sign-lang-translator.herokuapp.com/).

You may also build and deploy the web application with git, heroku CLI and node.js yourself by doing the following: 
1. Fork and clone this git repository.
2. Build the application with `heroku create $APPNAME --buildpack mars/create-react-app`, where `$APPNAME` will be the name of the app. **Note:** this will also be the subdomain name of the app in Heroku.
3. Commit all the changes in the repository as a result of the above command.
4. Deploy the app by pushing the repository to Heroku: `git push heroku master`. 

**Important:**
The web application makes use of an API found [here](https://github.com/dewald-els/noroff-assignment-api) that we need to deploy ourselves. This requires to configure and use an API key. When deploying, you need to specify the url to the API as well as a random API key as environment variables. In this app, the variables are called `REACT_APP_API_BASE_URL` and `REACT_APP_API_KEY` respectively. 

## Usage
- The app requires you to enter a name. An unused name will be registered. If a name is already registered, it will appear in the list.
- When a name is entered, you can access the translation page. Here, you can enter text to be translated to sign language. All entered input will be saved in history. You can always get to this page by clicking on the "Sign language translator" title page at the top-left as long as you have registered a name.
- When clicking the user icon at the top-right, you will go to the profile page. Here you can find the history of translations as well as an option to delete them. It is also possible to sign out via this page.

## License
Copyright &copy; 2022 Kevin Choi

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 
const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
const API_KEY = process.env.REACT_APP_API_KEY;
const DEFAULT_HEADERS = {"X-API-Key": API_KEY, "Content-type": "application/json"};

/**
 * GET request for user data.
 * @param userId The id of the user for which data must be requested.
 * When omitted, it will return a list with data of all users.
 * @returns {Promise<any>} A promise that returns the json of the GET request.
 * @throws {Error} If the request did not result in an ok response.
 */
export async function getUserData(userId="") {
    const response = await fetch(`${API_BASE_URL}/${userId}`);
    if (!response.ok) throw new Error(`loading user data failed.`);
    return await response.json();
}

/**
 * POST request to add new user.
 * If the user already exists, the data of the user will be returned and no POST request will be done.
 * @param username the username of the new user.
 * @returns {Promise<any>} A promise that returns the data of the new/existing user.
 * @throws {Error} If the request did not result in an ok response.
 * @throws {Error} If the username is not valid: must have at least 3 characters (spaces at the end will be trimmed).
 */
export async function addNewUser(username) {
    // Check if the username is valid:
    username = username.trim();
    if (username.length < 3) throw new Error("please enter a valid username with at least 3 characters.");

    // Check if the user already exists. If yes, return data instead:
    const users = await getUserData();
    const searchResult = users.find(user => user.username === username);
    if (searchResult) return searchResult;

    // The POST request:
    const response = await fetch(API_BASE_URL, {
        method: "POST",
        headers: DEFAULT_HEADERS,
        body: JSON.stringify({
            username: username,
            translations: [],
        })
    });
    if (!response.ok) throw new Error(`adding new user ${username} failed.`);
    return await response.json();
}

/**
 * PATCH request to modify the translation history of a user.
 * @param userId the id of the user for which the translation is history must be modified.
 * @param value the new value for the translation history. If not specified, it will be an empty array.
 * @returns {Promise<any>} A promise that returns the updated data of the specified user.
 * @throws {Error} If the id of the specified user does not exist.
 * @throws {Error} If the request did not result in an ok response.
 */
export async function updateTranslationHistory(userId, value=[]) {
    // Check if the user id exists:
    const users = await getUserData();
    const searchResult = users.find(user => user.id === parseInt(userId))
    if (!searchResult) throw new Error(`the user id ${userId} does not exist.`);

    // The PATCH request:
    const response = await fetch(`${API_BASE_URL}/${userId}`, {
        method: "PATCH",
        headers: DEFAULT_HEADERS,
        body: JSON.stringify({
            translations: value,
        })
    });
    if (!response.ok) throw new Error(`updating translation history of user ${searchResult.username} failed.`);
    return await response.json();
}
import TranslateHistory from "./TranslateHistory";
import SettingsButtons from "./SettingsButtons";

/**
 * The profile page component contains a translation history and buttons for sign-out and deleting history.
 * @returns {JSX.Element}
 * @constructor
 */
function ProfilePage() {
    return (
        <>
            <TranslateHistory/>
            <SettingsButtons/>
        </>
    );
}
export default ProfilePage;
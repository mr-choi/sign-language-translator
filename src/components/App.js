import NavbarComponent from "./NavbarComponent";
import MainComponent from "./MainComponent";
import {BrowserRouter} from "react-router-dom";
import {CookiesProvider} from "react-cookie";
import {CurrentUserProvider} from "../contexts/currentUserContext";
import TranslationsProvider from "../contexts/translationsContext";

/**
 * The main application wrapped in context providers, browser routers.
 * @returns {JSX.Element}
 * @constructor
 */
function App() {
    return (
        <BrowserRouter>
          <CookiesProvider>
              <CurrentUserProvider>
                  <NavbarComponent/>
                  <TranslationsProvider>
                      <MainComponent/>
                  </TranslationsProvider>
              </CurrentUserProvider>
          </CookiesProvider>
        </BrowserRouter>
    );
}

export default App;

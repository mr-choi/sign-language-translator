import {Button, Spinner} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {useCurrentUserContext} from "../contexts/currentUserContext";
import {useTranslationsContext} from "../contexts/translationsContext";
import {useState} from "react";

/**
 * The settings component with two buttons: deleting translation history and signing out.
 * @returns {JSX.Element}
 * @constructor
 */
function SettingsButtons() {
    // Contexts, states, and navigation hook:
    const currentUserContext = useCurrentUserContext();
    const translationsContext = useTranslationsContext();
    const [deleteTranslationsLoading, setDeleteTranslationsLoading] = useState(false);
    const navigate = useNavigate();

    /**
     * Event handle when the client presses delete translation history button.
     */
    function onDeleteHistory() {
        if (!window.confirm("Are you sure? This cannot be undone.")) return; // Make sure if user wants to do it.
        setDeleteTranslationsLoading(true);
        translationsContext.deleteTranslationHistory() // Only delete history if there were no errors with the API.
            .catch(error => alert(`Error while deleting translations: ${error.message}`))
            .finally(() => setDeleteTranslationsLoading(false))
    }

    /**
     * Event handle when the client presses the sign-out button.
     */
    function onSignOut() {
        currentUserContext.removeCurrentUser();
        navigate("/sign-in");
    }
    
    return (
        <>
            <Button variant="danger" className="me-2" onClick={onDeleteHistory}>Delete translation history</Button>
            <Button variant="primary" className="me-2" onClick={onSignOut}>Sign out</Button>
            {deleteTranslationsLoading && <Spinner animation="border" variant="primary"/>}
        </>
    );
}
export default SettingsButtons;
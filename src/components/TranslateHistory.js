import {useTranslationsContext} from "../contexts/translationsContext";
import {useNavigate} from "react-router-dom";
import {Button} from "react-bootstrap";

/**
 * Component that displays the history of the last 10 translations.
 * @returns {JSX.Element}
 * @constructor
 */
function TranslateHistory() {
    const translationsContext = useTranslationsContext();
    const navigate = useNavigate();

    /**
     * This function creates an event handler for translation item. The handle redirects the client to the index page
     * and shows the result of the translation.
     * @param index indication of the item in the translations list.
     * @returns {(function(): void)|*} the corresponding event handler.
     */
    function onTranslationItemClick(index) {
        return () => {
            translationsContext.setLastQuery(translationsContext.translationsList[index]);
            navigate("/");
        }
    }

    return (
        <>
            <h4>Translations history</h4>
            <ul className="mb-4">
                {translationsContext.translationsList.map((translation, index) =>
                    <li key={index}>
                        <Button variant="link" onClick={onTranslationItemClick(index)}>{translation}</Button>
                    </li>)}
                {!translationsContext.translationsList.length &&
                    <li className="text-secondary">No translations to show</li>}
            </ul>
        </>
    );
}
export default TranslateHistory;
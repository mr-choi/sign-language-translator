import {useLocation} from "react-router-dom";

/**
 * Simple no found component to be displayed when the client enters an invalid url.
 * @returns {JSX.Element}
 * @constructor
 */
function NoFoundPage() {
    const location = useLocation();
    return (
        <>
            <h4>Error</h4>
            <p>The page <code>{location.pathname}</code> does not exist.</p>
        </>
    )
}
export default NoFoundPage;
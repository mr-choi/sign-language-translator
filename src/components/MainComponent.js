import {Container} from "react-bootstrap";
import {Route, Routes, Navigate} from "react-router-dom";
import NoFoundPage from "./NoFoundPage";
import SignInPage from "./SignInPage";
import ProfilePage from "./ProfilePage";
import TranslatePage from "./TranslatePage";
import {useCurrentUserContext} from "../contexts/currentUserContext";

/**
 * A small component that makes sure that the child components will only load if access is granted.
 * Otherwise, the client should be redirected to the sign-in page.
 * @param children The child components that have to be protected.
 * @param access Whether the child components are accessible.
 * @returns {JSX.Element|*} The child components if access is granted, otherwise redirection to sign in page.
 * @constructor
 */
function ProtectedRoute({children, access}) {
    return !access ? <Navigate to="/sign-in" replace/> : children;
}

/**
 * The main components contains everything below the navigation bar.
 * @returns {JSX.Element}
 * @constructor
 */
function MainComponent() {
    // The profile and home page are accessible if the user id in de current user context is set.
    const currentUserState = useCurrentUserContext();

    return (
        <Container>
            <Routes>
                <Route path="/profile" element={<ProtectedRoute access={currentUserState.currentUserId}>
                    <ProfilePage />
                </ProtectedRoute>}/>
                <Route path="/sign-in" element={<SignInPage />}/>
                <Route path="/" element={<ProtectedRoute access={currentUserState.currentUserId}>
                    <TranslatePage/>
                </ProtectedRoute>}/>
                <Route path="/*" element={<NoFoundPage/>}/>
            </Routes>
        </Container>
    )
}
export default MainComponent;
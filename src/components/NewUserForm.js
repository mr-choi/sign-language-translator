import {Button, Form, Spinner} from "react-bootstrap";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import {addNewUser} from "../api/TranslationAPI";
import {useCurrentUserContext} from "../contexts/currentUserContext";

/**
 * Component that allows to register a new user.
 * @returns {JSX.Element}
 * @constructor
 */
function NewUserForm() {
    // Contexts, states, and navigation:
    const currentUserContext = useCurrentUserContext();
    const [typedUsername, setTypedUsername] = useState("");
    const [formError, setFormError] = useState("");
    const [formLoading, setFormLoading] = useState(false);
    const navigate = useNavigate();

    /**
     * Event handler when submitting a (new) username.
     * @param event
     */
    function onSubmitUser(event) {
        event.preventDefault(); // prevent GET request and page reload.
        setFormError("");
        setFormLoading(true);
        const input = typedUsername.trim();
        addNewUser(input) // The input should be posted to the API. Only continue if done successfully.
            .then(newUserItem => {
                currentUserContext.setCurrentUser(newUserItem.id, newUserItem.username);
                navigate("/");
            })
            .catch(error => setFormError(`Error: ${error.message}`))
            .finally(() => setFormLoading(false));
    }

    /**
     * Event handler when the client is typing in the input field.
     * @param event
     */
    function onChangeTypedUsername(event) {
        setTypedUsername(event.target.value); // value of `typedUsername` should be the same as text input.
        setFormError("");
    }

    return (
        <>
            <h4>Sign up new user</h4>
            <Form className="mb-4">
                <Form.Group controlId="username" as="fieldset" className="input-group">
                    <Form.Label visuallyHidden>Username</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Username"
                        value={typedUsername}
                        onChange={onChangeTypedUsername}
                    />
                    <Button variant="primary" type="submit" className="input-group-btn" onClick={onSubmitUser}>
                        Confirm
                    </Button>
                </Form.Group>
                {formLoading && <Spinner animation="border" variant="primary" />}
                {formError && <Form.Text className="text-danger">{formError}</Form.Text>}
            </Form>
        </>
    );
}
export default NewUserForm;
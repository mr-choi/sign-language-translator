import NewUserForm from "./NewUserForm";
import ExistingUserList from "./ExistingUserList";

/**
 * The component to be shown if the client needs to sign in.
 * @returns {JSX.Element}
 * @constructor
 */
function SignInPage() {
    return (
        <>
            <NewUserForm/>
            <ExistingUserList/>
        </>
    )
}
export default SignInPage;
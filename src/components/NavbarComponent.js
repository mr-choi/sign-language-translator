import {Container, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import {useCurrentUserContext} from "../contexts/currentUserContext";

/**
 * The navigation bar component at the top of the page.
 * @returns {JSX.Element}
 * @constructor
 */
function NavbarComponent() {

    // The current user's username set in the current user context will be displayed.
    const currentUserState = useCurrentUserContext();

    return (
        <Navbar variant={"dark"} bg={"dark"} className="shadow mb-3">
            <Container>
                <Link to="/" className="navbar-brand">Sign Language Translator</Link>
                <Navbar.Collapse>
                    <Navbar.Text className="ms-auto">
                        <Nav.Link className="py-0">
                            <Link to="/profile">
                                {currentUserState.currentUsername} <i className="bi bi-person-fill"/>
                            </Link>
                        </Nav.Link>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
export default NavbarComponent;
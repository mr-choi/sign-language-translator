import {useEffect, useState} from "react";
import {getUserData} from "../api/TranslationAPI";
import {useCurrentUserContext} from "../contexts/currentUserContext";
import {Button, Spinner} from "react-bootstrap";
import {useNavigate} from "react-router-dom";

/**
 * Component that lists all the users that exists in the API.
 * @returns {JSX.Element}
 * @constructor
 */
function ExistingUserList() {
    // Contexts, states, and navigation:
    const currentUserContext = useCurrentUserContext();
    const [existingUsersList, setExistingUsersList] = useState([]);
    const [errorMessage, setErrorMessage] = useState("");
    const [loadingUsersList, setLoadingUsersList] = useState(false);
    const navigate = useNavigate();

    // This effect will get the existing users list via the API when the page is loaded or if the current user changes.
    useEffect(() => {
        setLoadingUsersList(true);
        getUserData()
            .then(userData => {
                setExistingUsersList(userData);
                setErrorMessage("");
            })
            .catch(error => {
                setErrorMessage(`Error: ${error.message}`);
            })
            .finally(() => setLoadingUsersList(false));
    }, [currentUserContext.currentUserId]);

    /**
     * Generate an event handler for the existing users in the list. When clicked, the selected user will be logged in.
     * @param id The id of the user.
     * @param username The username of the user.
     * @returns {(function(): void)|*} The event handler for the specified user.
     */
    function onExistingUserClick(id, username) {
        return () => {
            currentUserContext.setCurrentUser(id, username);
            navigate("/");
        }
    }

    /**
     * Helper function that renders the jsx of the user list. If logged in, the current user will appear in bold.
     * @param userList The user list that must be rendered.
     * @returns {*} The resulting jsx.
     */
    function renderUserList(userList) {
        return userList.map((userData) => {
            const className = (userData.id === parseInt(currentUserContext.currentUserId)) ? "fw-bold" : "";
            return (
                <li key={userData.id} className={className}>
                    <Button variant="link" onClick={onExistingUserClick(userData.id, userData.username)}>
                        {userData.username}
                    </Button>
                </li>
            )
        });
    }

    return (
        <>
            <h4>Existing users</h4>
            <ul>
                {renderUserList(existingUsersList)}
            </ul>
            {errorMessage && <span className="text-danger">{errorMessage}</span> }
            {loadingUsersList && <Spinner animation="border" variant="primary"/>}
        </>
    );
}
export default ExistingUserList;
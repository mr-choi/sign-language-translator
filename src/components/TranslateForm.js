import {Button, Form, Spinner} from "react-bootstrap";
import {useTranslationsContext} from "../contexts/translationsContext";
import {useState} from "react";

/**
 * Component with input field to enter text to translate into sign language.
 * @returns {JSX.Element}
 * @constructor
 */
function TranslateForm() {
    // Contexts and translations:
    const translationsContext = useTranslationsContext();
    const [enteredTranslation, setEnteredTranslation] = useState("");
    const [translationLoading, setTranslationLoading] = useState(false);
    const [translationError, setTranslationError] = useState("");

    /**
     * Event handle when user types in the input field. Make sure that state value is equal to text input.
     * @param event
     */
    function onEnteredTextChange(event) {
        setEnteredTranslation(event.target.value);
        setTranslationError("");
        translationsContext.setLastQuery("");
    }

    /**
     * Event handle when client clicks the translate button.
     * @param event
     */
    function onTranslate(event) {
        event.preventDefault(); // Prevent page reload.
        setTranslationError("");
        setTranslationLoading(true);
        // Only add translation in state, if the API code runs without errors.
        translationsContext.addTranslation(enteredTranslation.trim())
            .catch(error => setTranslationError(`Error while adding translation: ${error.message}`))
            .finally(() => setTranslationLoading(false));
    }

    return (
        <>
            <h4>Translate text into sign language</h4>
            <Form className="mb-4">
                <Form.Group controlId="text-to-translate" as="fieldset" className="input-group">
                    <Form.Label visuallyHidden>Enter text</Form.Label>
                    <Form.Control type="text" placeholder="Enter text" value={enteredTranslation}
                                  onChange={onEnteredTextChange} />
                    <Button variant="primary"
                            type="submit"
                            className="input-group-btn"
                            onClick={onTranslate}>Translate</Button>
                </Form.Group>
                {translationLoading && <Spinner animation="border" variant="primary" />}
                <Form.Text className="text-danger">{translationError}</Form.Text>
            </Form>
        </>
    );
}
export default TranslateForm;
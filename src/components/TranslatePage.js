import TranslateForm from "./TranslateForm";
import TranslateResult from "./TranslateResult";

/**
 * Component that displays the index page.
 * @returns {JSX.Element}
 * @constructor
 */
function TranslatePage() {
    return (
        <>
            <TranslateForm/>
            <TranslateResult/>
        </>
    );
}
export default TranslatePage;
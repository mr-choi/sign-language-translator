import {Image} from "react-bootstrap";
import {useTranslationsContext} from "../contexts/translationsContext";

/**
 * Component that displays the result of a translation.
 * @returns {JSX.Element}
 * @constructor
 */
export function TranslateResult() {

    const translationsContext = useTranslationsContext();
    // Only alphabetical letters will be interpreted. Translation is case-insensitive.
    const letters = translationsContext.lastQuery.toLowerCase().match(/[a-z]/g) || [];

    /**
     * Obtain the sign language image of a specified letter.
     * @param letter - the letter.
     * @returns {*} - the corresponding sign language image.
     */
    function getSignImage(letter) {
        return require(`../sign_images/${letter.toLowerCase()}.png`);
    }

    return (
        <>
            <ul className="list-inline">
                {letters.map(letter => <li className="list-inline-item" key={letter}>
                    <Image width="64px" alt={letter} src={getSignImage(letter)}/>
                </li>)}
            </ul>
        </>
    );
}
export default TranslateResult;
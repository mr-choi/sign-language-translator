import {createContext, useContext, useEffect, useState} from "react";
import {useCurrentUserContext} from "./currentUserContext";
import {getUserData, updateTranslationHistory} from "../api/TranslationAPI";
import {Container, Spinner} from "react-bootstrap";

const TranslationsContext = createContext({});

// Custom context-specific hook:
export function useTranslationsContext() {
    return useContext(TranslationsContext);
}

/**
 * Provider that enables child components to make use of the translation history data of the current user.
 * @param children
 * @returns {JSX.Element}
 * @constructor
 */
function TranslationsProvider({children}) {
    // Contexts and states.
    const currentUserContext = useCurrentUserContext();
    const [translationsList, setTranslationsList] = useState([]);
    const [lastQuery, setLastQuery] = useState(""); // Last query equals the result displayed in sign language.
    const [loading, setLoading] = useState(false);

    /**
     * Add a new translation in the history of the current user. The history list length will be capped at 10.
     * @param query The input.
     * @returns {Promise<void>} A promise with no return value.
     * @throws {Error} If the input is empty or contains more than 40 characters.
     * @throws {Error} If updating the translation history via the API ends with an error.
     */
    async function addTranslation(query) {
        setLastQuery("");
        query = query.trim();
        if (!query) throw new Error("please enter a translation.");
        if (query.length > 40) throw new Error("please only enter 40 characters max.")
        const newList = [query, ...translationsList];
        if (newList.length > 10) newList.pop(); // Only the last 10 translations will be used.
        await updateTranslationHistory(currentUserContext.currentUserId, newList);
        setTranslationsList(newList);
        setLastQuery(query);
    }

    /**
     * Delete the translation history of the current user.
     * @returns {Promise<void>} A promise with no return value.
     * @throws {Error} If updating the translation history via the API ends with an error.
     */
    async function deleteTranslationHistory() {
        await updateTranslationHistory(currentUserContext.currentUserId, []);
        setTranslationsList([]);
        setLastQuery("");
    }

    /**
     * This effect will get the translations from the API when the page is loading or the current user changes.
     */
    useEffect(() => {
        setLoading(true);
        setLastQuery("");
        if (!currentUserContext.currentUserId) { // In case the user is signed out, no GET request will be performed.
            setLoading(false);
            setTranslationsList([]);
        } else { // Otherwise, the data of the user signed in will be loaded.
            getUserData(currentUserContext.currentUserId)
                .then(result => {
                    setTranslationsList(result.translations);
                })
                .catch(error => {
                    alert(`Error while loading translations: ${error.message}`);
                })
                .finally(() => setLoading(false));
        }
    }, [currentUserContext.currentUserId]);

    const state = {
        translationsList,
        addTranslation,
        deleteTranslationHistory,
        lastQuery,
        setLastQuery,
    }

    return (
        <TranslationsContext.Provider value={state}>
            {loading ? <Container className="text-center"><Spinner animation="border" variant="primary"/></Container> : children}
        </TranslationsContext.Provider>
    )
}
export default TranslationsProvider;
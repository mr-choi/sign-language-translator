import {createContext, useContext} from "react";
import {useCookies} from "react-cookie";

const CurrentUserContext = createContext({});

// Custom context-specific hook:
export function useCurrentUserContext() {
    return useContext(CurrentUserContext);
}

/**
 * This provider allows the child components to make use state information with regard to the current user signed in.
 * @param children
 * @returns {JSX.Element}
 * @constructor
 */
export function CurrentUserProvider({children}) {
    const [cookies, setCookie, removeCookie] = useCookies(["currentUserId", "currentUsername"]);

    /**
     * Sign in the user by saving its id and username.
     * @param newCurrentUserId the id of the user to be signed in.
     * @param newCurrentUsername the username of the user.
     */
    function setCurrentUser(newCurrentUserId, newCurrentUsername) {
        setCookie("currentUserId", newCurrentUserId, {sameSite: "strict"});
        setCookie("currentUsername", newCurrentUsername, {sameSite: "strict"});
    }

    /**
     * Sign out the current user by deleting the id and username.
     */
    function removeCurrentUser() {
        removeCookie("currentUserId", {sameSite: "strict"});
        removeCookie("currentUsername", {sameSite: "strict"});
    }

    const state = {
        currentUserId: cookies.currentUserId,
        currentUsername: cookies.currentUsername,
        setCurrentUser: setCurrentUser,
        removeCurrentUser: removeCurrentUser,
    }

    return (
        <CurrentUserContext.Provider value={state}>
            {children}
        </CurrentUserContext.Provider>
    )
}